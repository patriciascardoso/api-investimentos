package br.com.InvestimentosApi.services;

import br.com.InvestimentosApi.DTOs.SimulacaoDTO;
import br.com.InvestimentosApi.models.Investimento;
import br.com.InvestimentosApi.models.Simulacao;
import br.com.InvestimentosApi.repositories.InvestimentoRepository;
import br.com.InvestimentosApi.repositories.SimulacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SimulacaoService {

    @Autowired
    private InvestimentoRepository investimentoRepository;

    @Autowired
    private SimulacaoRepository simulacaoRepository;

    @Autowired
    private InvestimentoService investimentoService;


    public SimulacaoDTO simularInvestimento (int idInvestimento, Simulacao simulacao){
        SimulacaoDTO simulacaoDTO = new SimulacaoDTO();

        Optional<Investimento> investimentoOptional = investimentoRepository.findById(idInvestimento);

        if (investimentoOptional.isPresent()){
            Investimento investimentoBD = investimentoOptional.get();
            simulacao.setInvestimento(investimentoBD);
            simulacaoRepository.save(simulacao);

            double rendimentoPorMes = simulacao.getValorAplicado() * simulacao.getInvestimento().getRendimentoAoMes();
            simulacaoDTO.setRendimentoPorMes(rendimentoPorMes);

            double montante = simulacao.getValorAplicado() + (simulacao.getQuantidadeMeses() * rendimentoPorMes);
            simulacaoDTO.setMontante(montante);

            return simulacaoDTO;

        }else {
            throw new RuntimeException("O investimento não foi encontrado");
        }

    }

    public List<Simulacao> consultarSimulacoes(){
        return (List<Simulacao>) simulacaoRepository.findAll();
    }
}
