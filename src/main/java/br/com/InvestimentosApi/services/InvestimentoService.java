package br.com.InvestimentosApi.services;

import br.com.InvestimentosApi.models.Investimento;
import br.com.InvestimentosApi.repositories.InvestimentoRepository;
import br.com.InvestimentosApi.repositories.SimulacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class InvestimentoService {

    @Autowired
    InvestimentoRepository investimentoRepository;

    @Autowired
    SimulacaoRepository simulacaoRepository;

    public Investimento cadastrarInvestimento(Investimento investimento){
        return investimentoRepository.save(investimento);
    }

    public Iterable<Investimento> consultarInvestimentos(){
        return investimentoRepository.findAll();
    }


}
