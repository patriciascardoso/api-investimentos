package br.com.InvestimentosApi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InvestimentosApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(InvestimentosApiApplication.class, args);
	}

}
