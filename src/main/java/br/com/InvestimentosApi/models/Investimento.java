package br.com.InvestimentosApi.models;


import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
public class Investimento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(unique = true)
    @NotNull(message = "Nome do Investimento não pode ser nullo")
    @NotBlank (message = "Nome do Investimento não pode estar em branco")
    private String nome;

    @NotNull (message = "Rendimento não pode ser nullo")
    @Digits(integer = 6, fraction = 2, message = "Rendimento fora do padrão")
    private double rendimentoAoMes;


    public Investimento() {
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getRendimentoAoMes() {
        return rendimentoAoMes;
    }

    public void setRendimentoAoMes(double rendimentoAoMes) {
        this.rendimentoAoMes = rendimentoAoMes;
    }
}
