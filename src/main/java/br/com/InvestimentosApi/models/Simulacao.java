package br.com.InvestimentosApi.models;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
public class Simulacao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    //@NotBlank(message = "Nome precisa ser preenchido")
    //@NotNull (message = "Nome precisa ser preenchido")
    private String nomeInteressado;

    //@NotBlank(message = "Email precisa ser preenchido")
    //@NotNull (message = "Email precisa ser preenchido")
    private String emailInteressado;


    //@NotNull(message = "Valor de Aplicação não pode ser nullo")
    private double valorAplicado;

    //@NotBlank(message = "Campo quantidade de meses precisa ser preenchido")
    //@NotNull (message = "Campo quantidade de meses precisa ser preenchido")
    private int quantidadeMeses;

    @ManyToOne
    private Investimento investimento;

    public Simulacao() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNomeInteressado() {
        return nomeInteressado;
    }

    public void setNomeInteressado(String nomeInteressado) {
        this.nomeInteressado = nomeInteressado;
    }

    public String getEmailInteressado() {
        return emailInteressado;
    }

    public void setEmailInteressado(String emailInteressado) {
        this.emailInteressado = emailInteressado;
    }

    public double getValorAplicado() {
        return valorAplicado;
    }

    public void setValorAplicado(double valorAplicado) {
        this.valorAplicado = valorAplicado;
    }

    public int getQuantidadeMeses() {
        return quantidadeMeses;
    }

    public void setQuantidadeMeses(int quantidadeMeses) {
        this.quantidadeMeses = quantidadeMeses;
    }

    public Investimento getInvestimento() {
        return investimento;
    }

    public void setInvestimento(Investimento investimento) {
        this.investimento = investimento;
    }
}
