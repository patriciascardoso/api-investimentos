package br.com.InvestimentosApi.controllers;


import br.com.InvestimentosApi.models.Investimento;
import br.com.InvestimentosApi.models.Simulacao;
import br.com.InvestimentosApi.repositories.SimulacaoRepository;
import br.com.InvestimentosApi.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping ("/simulacoes")
public class SimulacaoController {

    @Autowired
    SimulacaoService simulacaoService;

    @GetMapping
    public  Iterable<Simulacao> consultarTodosSimulacoes(){
        return simulacaoService.consultarSimulacoes();
    }


}
