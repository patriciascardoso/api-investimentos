package br.com.InvestimentosApi.controllers;

import br.com.InvestimentosApi.DTOs.SimulacaoDTO;
import br.com.InvestimentosApi.models.Investimento;
import br.com.InvestimentosApi.models.Simulacao;
import br.com.InvestimentosApi.services.InvestimentoService;
import br.com.InvestimentosApi.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/investimentos")

public class InvestimentoController {

    @Autowired
    private InvestimentoService investimentoService;

    @Autowired
    private SimulacaoService simulacaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Investimento cadastrarNovoInvestimento(@RequestBody @Valid Investimento investimento){
        return investimentoService.cadastrarInvestimento(investimento);
    }

    @GetMapping
    public  Iterable<Investimento> consultarTodosInvestimentos(){
        return investimentoService.consultarInvestimentos();
    }

    @PostMapping ("/{idInvestimento}/simulacao")
    @ResponseStatus(HttpStatus.CREATED)
    public SimulacaoDTO cadastrarSimulacaoInvestimento(@PathVariable(name = "idInvestimento") int idInvestimento,
                                                       @RequestBody  Simulacao simulacao){
     try {
         return simulacaoService.simularInvestimento(idInvestimento, simulacao);
     } catch (RuntimeException e){
         throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
     }
    }
}


