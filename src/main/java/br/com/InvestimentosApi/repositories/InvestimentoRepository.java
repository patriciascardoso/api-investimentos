package br.com.InvestimentosApi.repositories;

import br.com.InvestimentosApi.models.Investimento;
import org.springframework.data.repository.CrudRepository;

public interface InvestimentoRepository extends CrudRepository <Investimento, Integer> {
}
