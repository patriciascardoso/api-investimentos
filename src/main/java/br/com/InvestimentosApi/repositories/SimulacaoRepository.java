package br.com.InvestimentosApi.repositories;

import br.com.InvestimentosApi.models.Simulacao;
import org.springframework.data.repository.CrudRepository;

public interface SimulacaoRepository extends CrudRepository <Simulacao, Integer> {
}
